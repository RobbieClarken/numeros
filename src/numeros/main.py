import random

from numeros.conversion import to_spanish

MIN_N = 0
MAX_N = 999_999


def main():
    while True:
        ask_a_question()


def ask_a_question():
    n = random.randint(MIN_N, MAX_N)
    correct_answer = to_spanish(n)
    guess = input(f"{n:,}\n")
    if guess == correct_answer:
        print("✓\n")
    else:
        print(f"✗\n{correct_answer}\n")
