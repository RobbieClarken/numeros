def to_spanish(n: int) -> str:
    match n:
        case _ if n < 0:
            raise NotImplementedError("negative numbers are not supported")
        case 0:
            return "cero"
        case 1:
            return "uno"
        case 2:
            return "dos"
        case 3:
            return "tres"
        case 4:
            return "cuatro"
        case 5:
            return "cinco"
        case 6:
            return "seis"
        case 7:
            return "siete"
        case 8:
            return "ocho"
        case 9:
            return "nueve"
        case 10:
            return "diez"
        case 11:
            return "once"
        case 12:
            return "doce"
        case 13:
            return "trece"
        case 14:
            return "catorce"
        case 15:
            return "quince"
        case 16:
            return "dieciseis"
        case 17:
            return "diecisiete"
        case 18:
            return "dieciocho"
        case 19:
            return "diecinueve"
        case 20:
            return "veinte"
        case 30:
            return "treinta"
        case 40:
            return "cuarenta"
        case 50:
            return "cincuenta"
        case 60:
            return "sesenta"
        case 70:
            return "setenta"
        case 80:
            return "ochenta"
        case 90:
            return "noventa"
        case _ if n < 30:
            return "veinti" + to_spanish(n - 20)
        case _ if n < 100:
            r = n % 10
            return to_spanish(n - r) + " y " + to_spanish(r)
        case 100:
            return "cien"
        case 200:
            return "doscientos"
        case 300:
            return "trescientos"
        case 400:
            return "cuatrocientos"
        case 500:
            return "quinientos"
        case 600:
            return "seiscientos"
        case 700:
            return "setecientos"
        case 800:
            return "ochocientos"
        case 900:
            return "novecientos"
        case _ if n < 200:
            return "ciento " + to_spanish(n - 100)
        case _ if n < 1_000:
            r = n % 100
            return to_spanish(n - r) + " " + to_spanish(r)
        case 1_000:
            return "mil"
        case _ if n < 2_000:
            r = n % 1_000
            return to_spanish(n - r) + " " + to_spanish(r)
        case _ if n < 1_000_000:
            num_of_thousand = n // 1_000
            r = n % 1_000
            return to_spanish(num_of_thousand) + " " + to_spanish(1_000 + r)
        case _:
            raise NotImplementedError()
